package ru.mtumanov.tm.command.data;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import org.jetbrains.annotations.NotNull;

import ru.mtumanov.tm.dto.Domain;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.data.DataSaveException;

public class DataXmlSaveFasterXmlCommand extends AbstractDataCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Save data to xml file";
    }

    @Override
    @NotNull
    public String getName() {
        return "data-save-xml";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[DATA SAVE XML]");
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_XML);
        try {
            Files.deleteIfExists(file.toPath());
            Files.createFile(file.toPath());
            @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
            @NotNull final ObjectMapper objectMapper = new XmlMapper();
            @NotNull final String xml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
            fileOutputStream.write(xml.getBytes());
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (IOException e) {
            throw new DataSaveException();
        }
    }
    
}
