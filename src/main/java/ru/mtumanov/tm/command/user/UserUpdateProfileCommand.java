package ru.mtumanov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

public class UserUpdateProfileCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "update current user profile";
    }

    @Override
    @NotNull
    public String getName() {
        return "update-user-profile";
    }

    @Override
    public void execute() throws AbstractException {
        @NotNull final String userId = getUserId();
        System.out.println("[USER UPDATE PROFILE]");
        System.out.println("ENTER FIRST NAME:");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER LAST NAME:");
        @NotNull final String lastName = TerminalUtil.nextLine();
        System.out.println("ENTER MIDDLE NAME:");
        @NotNull final String middleName = TerminalUtil.nextLine();
        serviceLocator.getUserService().userUpdate(userId, firstName, lastName, middleName);
    }

}
