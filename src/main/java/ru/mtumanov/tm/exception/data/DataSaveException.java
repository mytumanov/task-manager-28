package ru.mtumanov.tm.exception.data;

public class DataSaveException extends AbstractDataException {
    
    public DataSaveException() {
        super("ERROR! Data save exception!");
    }
    
}
