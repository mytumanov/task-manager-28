package ru.mtumanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.api.repository.IUserRepository;
import ru.mtumanov.tm.exception.user.UserNotFoundException;
import ru.mtumanov.tm.model.User;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    @NotNull
    public User findByLogin(@NotNull final String login) throws UserNotFoundException {
        return models.stream()
                .filter(m -> login.equals(m.getLogin()))
                .findFirst()
                .orElseThrow(UserNotFoundException::new);
    }

    @Override
    @NotNull
    public User findByEmail(@NotNull final String email) throws UserNotFoundException {
        return models.stream()
                .filter(m -> email.equals(m.getLogin()))
                .findFirst()
                .orElseThrow(UserNotFoundException::new);
    }

    @Override
    public boolean isLoginExist(@NotNull final String login) {
        return models.stream()
                .anyMatch(m -> login.equals(m.getLogin()));
    }

    @Override
    public boolean isEmailExist(@NotNull final String email) {
        return models.stream()
                .anyMatch(m -> email.equals(m.getLogin()));
    }

}
